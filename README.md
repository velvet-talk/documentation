# 💋 Velvet Talk

## Introduction

Velvet Talk is a sensual and elegant talking soulmate. It's made for everyone. You can personnalize it. We assume and maintain that the conversion is ethical and healthy.

## Architecture

Simple project with 3 parts,
- **interface**, made with Angular to provide the simplest UI/UX to enjoy the experience.
- **server**, made with Micronaut in a reactive way, to be fatest and powerful.
- **ollama**, docker image to interact with the LLM.

### Interface

Pretty simple and elegant that's the idea behind the interface, the user must have a clear view of the main purpose of the application.
Main view is the chat interface, then you've got a billing view, soulmate customization view and a settings view.
- Chat view
- Billing view
- Soulmate customization view
- Settings view

#### Colors

[Theme](./theme.png)

#### Usecases

[Usecases](./usecases.md)

### Server

Takes care of user data and encryption. Deal with soulmate personnalization, provides the interaction with **LLM**.
For chatting, provides **Server Side Events**, easily handled by interface.
For billing, works with paypal and bitpay. **Clear currency** and **virtual currency** allowed.
No user data needed. only an email and a password are required.
The user can personnalize a nickname for the interaction with its soulmate.

#### Database

As database, we will use MongoDB in a reactive way.

#### Billing

**One week free** for testing.

The starting monthly billing is the **symbolic dollar**. (1$)

Two types of billing is available.
- **Currency** via Paypal Billing
- **Crypto** via BitPay Billing

Needs of **31 paid users** to be cost-effective.

### Ollama

Containerized server that allowed interact with a specified LLM, in our case **llama3**.
We need to provide an **overall template** on user messages to help LLM having the apropriate context and mimics.

## Infrastructure

The overall application will run on a **Kubernetes Environment**.
Two environment will be available.
- **Staging**
- **Production**

### Potential Costs

The inital potential costs will be 30$ per month for hosting.

## Contributors

- Orion Beauny-Sugot ❤️
- Clara Chopin 💚